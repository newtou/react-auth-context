const router = require('express').Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const {
  registerValidation,
  loginValidation
} = require('../validation/validate')

// get token secret
const tokenSecret = process.env.TOKEN_SECRET

router.post('/register', async (req, res) => {
  // validate our data
  const { error } = registerValidation(req.body)
  if (error) return res.status(400).send(error.details[0].message)

  // check if user exists
  const emailExists = await User.findOne({ email: req.body.email })
  if (emailExists) return res.status(400).send('Email already exists')

  // hash password
  const salt = await bcrypt.genSalt(12)
  const hashPassword = await bcrypt.hash(req.body.password, salt)

  // create a new user
  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword
  })

  try {
    const savedUser = await user.save()
    const token = jwt.sign(
      { id: savedUser._id, email: savedUser.email },
      tokenSecret,
      { expiresIn: '1min' }
    )
    const serializedToken = JSON.stringify(token)
    return res
      .header('token', serializedToken)
      .send(serializedToken)
      .status(200)
  } catch (err) {
    console.log(err)
    return res.status(400).send({ msg: 'something went wrong' })
  }
})

router.post('/login', async (req, res) => {
  // validation
  const { error } = loginValidation(req.body)
  if (error) {
    return res.status(400).send(error.details[0].message)
  }

  // check if user exists
  const user = await User.findOne({ email: req.body.email })
  if (!user) return res.status(400).send('Invalid credentials')

  // check if passwords match
  const validPass = await bcrypt.compare(req.body.password, user.password)
  if (!validPass) return res.status(400).send('Invalid credentials')

  // create && assign token
  const token = jwt.sign({ id: user._id, email: user.email }, tokenSecret, {
    expiresIn: 60
  })
  const serializedToken = JSON.stringify(token)
  return res
    .header('token', serializedToken)
    .send(serializedToken)
    .status(200)
})

module.exports = router
