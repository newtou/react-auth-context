import Lockr from 'lockr'
import axios from 'axios'

import { USER, BASE_URL, ACCESS_TOKEN } from '../constants'

async function invoke(
  method,
  path,
  data = undefined,
  options = {
    noAuth: undefined,
    timeout: 25000,
    headers: {}
  }
) {
  const headers = {
    ...options.headers
  }

  const accessToken = Lockr.get(ACCESS_TOKEN)
  if (accessToken) {
    headers.Authorization = `Bearer ${Lockr.get(ACCESS_TOKEN)}`
  }

  const opts = {
    method,
    mode: 'cors',
    headers
  }

  if (data) {
    opts.headers['Content-Type'] = 'application/json'
    opts.body = JSON.stringify(data)
  }

  let res = {}

  try {
    const url = `${BASE_URL}${path}`.replace(/\+/, '%2B').replace(/ /, '%20')

    const { timeout } = options

    res = await Promise.race([
      fetch(url, opts),
      new Promise((resolve, reject) =>
        setTimeout(() => {
          reject(new Error('timeout'))
        }, timeout)
      )
    ])
  } catch (ex) {
    throw ex
  }

  if (!(res.status >= 200 && res.status < 300)) {
    if (res.status === 401) {
      Lockr.rm(ACCESS_TOKEN)
      Lockr.rm(USER)
      return null
    }
    throw res.statusText
  }

  const result = await res.json()
  if (result.metadata) {
    return {
      data: result.data
    }
  }
  return result.data
}

export default invoke
