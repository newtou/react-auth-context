import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import history from '../routes/history'
import { logout } from '../localstorage/LocalStorage'
import { AuthContext } from '../context/AuthContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

function Header() {
  const [isAuthenticated, setIsAuthenticated] = useContext(AuthContext)
  const handleClick = async e => {
    logout()
    setIsAuthenticated(false)
    history.push('/login')
  }
  return (
    <div className='nav--bar'>
      <div className='nav--wrapper'>
        <div className='nav--logo'>
          <h4>
            <strong>React Con-API</strong>
          </h4>
        </div>
        {isAuthenticated ? (
          <div className='nav--links'>
            <Link to='/' className='nav--item'>
              Home
            </Link>
            <Link to='/' className='nav--item'>
              other link
            </Link>
            <Link to='/' className='nav--item'>
              other link
            </Link>
            <span style={{ marginLeft: '2rem' }}>
              <button className='btn btn--logout' onClick={handleClick}>
                <FontAwesomeIcon
                  style={{ color: 'blue' }}
                  icon={faSignOutAlt}></FontAwesomeIcon>
              </button>
            </span>
          </div>
        ) : (
          <div className='nav--links'>
            <Link to='/' className='nav--item'>
              Home
            </Link>
          </div>
        )}
      </div>
    </div>
  )
}

export default Header
