import React, { useState, Fragment, useContext } from 'react'
import Header from '../components/Header'
import { Link } from 'react-router-dom'
import { register } from '../localstorage/LocalStorage'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserAlt, faEnvelope, faKey } from '@fortawesome/free-solid-svg-icons'
import { AuthContext } from '../context/AuthContext'

function Register({ history }) {
  const [Null, setIsAuthenticated] = useContext(AuthContext)
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState(false)

  const handleSubmit = async e => {
    e.preventDefault()
    // do some form validation later
    setLoading(false)
    try {
      const token = await register(name, email, password)
      setLoading(false)
      if (token) {
        setIsAuthenticated(true)
        history.push('/dashboard')
      }
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  return (
    <Fragment>
      <Header></Header>
      <div className='register--container'>
        <div className='register--form'>
          <form onSubmit={handleSubmit} method='POST'>
            <h3 className='register--title'>Create your account</h3>
            <br />
            <hr />
            <br />
            <div className='form--group'>
              <label htmlFor='name'>Name</label>
              <input
                type='text'
                name='name'
                id='name'
                className='form--control'
                placeholder='name'
                onChange={e => setName(e.target.value)}
              />
              <FontAwesomeIcon
                className='icon icon--name'
                icon={faUserAlt}></FontAwesomeIcon>
            </div>
            <div className='form--group'>
              <label htmlFor='email'>Email</label>
              <input
                type='email'
                email='email'
                id='email'
                placeholder='email'
                className='form--control'
                onChange={e => setEmail(e.target.value)}
              />
              <FontAwesomeIcon
                className='icon icon-email'
                icon={faEnvelope}></FontAwesomeIcon>
            </div>
            <div className='form--group'>
              <label htmlFor='password'>Password</label>
              <input
                type='password'
                password='password'
                id='password'
                className='form--control'
                placeholder='password'
                onChange={e => setPassword(e.target.value)}
              />
              <FontAwesomeIcon
                className='icon icon-password'
                icon={faKey}></FontAwesomeIcon>
            </div>
            <span>
              <p style={{ margin: '10px 5px' }}>
                <small>
                  Have an account? <Link to='/login'>Sign in</Link>
                </small>
              </p>
            </span>
          </form>
          <div className='form--group'>
            <button
              type='submit'
              className='btn register--btn'
              onClick={handleSubmit}>
              {loading ? <span>loading ...</span> : <span>Sign Up</span>}
            </button>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Register
