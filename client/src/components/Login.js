import React, { useState, Fragment, useContext } from 'react'
import Header from '../components/Header'
import { Link } from 'react-router-dom'
import { login } from '../localstorage/LocalStorage'
import { AuthContext } from '../context/AuthContext'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faKey } from '@fortawesome/free-solid-svg-icons'
import Loader from 'react-loader-spinner'

function Login({ history }) {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [msg, setMsg] = useState('')
  const [loading, setLoading] = useState(false)
  const [Null, setIsAuthenticated] = useContext(AuthContext)

  const validate = () => {
    if (!email) {
      setMsg('This field is required')
      setTimeout(() => {
        setMsg('')
      }, 5000)
      return false
    } else if (!email.includes('@')) {
      setMsg('Email is invalid')
      setTimeout(() => {
        setMsg('')
      }, 5000)
      return false
    } else if (!password) {
      setMsg('This field is required')
      setTimeout(() => {
        setMsg('')
      }, 5000)
      return false
    }
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setLoading(true)
    validate()
    try {
      const token = await login(email, password)
      setLoading(false)
      if (token) {
        setIsAuthenticated(true)
        history.push('/dashboard')
      }
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  return (
    <Fragment>
      <Header></Header>
      <div className='login--container'>
        <div className='login--form'>
          <form method='POST'>
            {msg ? (
              <div
                style={{
                  padding: '10px',
                  width: '100%',
                  borderRadius: '4px',
                  textAlign: 'center',
                  color: 'white',
                  background: '#eb1c5a29',
                  border: '2px solid #eb1c56',
                  marginBottom: '1rem'
                }}>
                {msg}
              </div>
            ) : (
              ''
            )}
            <h3 className='register--title'>Login to your account</h3>
            <br />
            <hr />
            <br />
            <div className='form--group'>
              <label htmlFor='email'>Email</label>
              <input
                type='email'
                name='email'
                id='email'
                className='form--control'
                placeholder='email'
                onChange={e => setEmail(e.target.value)}
                required
              />
              <FontAwesomeIcon
                className='icon icon--email'
                icon={faEnvelope}></FontAwesomeIcon>
            </div>
            <div className='form--group'>
              <label htmlFor='password'>Password</label>
              <input
                type='password'
                name='password'
                id='password'
                className='form--control'
                placeholder='password'
                onChange={e => setPassword(e.target.value)}
                required
              />
              <FontAwesomeIcon
                className='icon icon--password'
                icon={faKey}></FontAwesomeIcon>
            </div>

            <span>
              <p style={{ margin: '10px 5px' }}>
                <small>
                  Don't have account?{' '}
                  <Link to='/register' style={{ color: '#000411' }}>
                    Sign up
                  </Link>
                </small>
              </p>
            </span>
          </form>

          <button
            type='submit'
            className='btn login--btn'
            onClick={handleSubmit}>
            {loading ? (
              <span>
                <Loader type='ThreeDots' color='#fff' height='45' width='45'>
                  loading
                </Loader>
              </span>
            ) : (
              <span>Sign In</span>
            )}
          </button>
        </div>
      </div>
    </Fragment>
  )
}

export default Login
