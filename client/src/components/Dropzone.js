import React, { useCallback } from 'react'
import { useDropzone } from 'react-dropzone'

function Dropzone() {
  const onDrop = useCallback(acceptedFiles => {
    alert(acceptedFiles[0].name)
  }, [])
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop
  })
  return (
    <div
      {...getRootProps()}
      style={{
        height: '200px',
        border: '0.5px dotted #eee',
        padding: '20px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
      <input {...getInputProps()}></input>
      {isDragActive ? (
        <p>Dropping ... </p>
      ) : (
        <p>Drag 'n' drop here, or click to select files</p>
      )}
    </div>
  )
}

export default Dropzone
