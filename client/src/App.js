import React, { Fragment, useContext } from 'react'
import './App.css'
import { Link } from 'react-router-dom'
import Header from './components/Header'
import { AuthContext } from './context/AuthContext'

function App() {
  const [isAuthenticated, Null] = useContext(AuthContext)
  return (
    <Fragment>
      <Header></Header>
      <div className='home--container'>
        <div className='home--content'>
          <h2>Welcome to our home page</h2>
          <br />
          <hr />
          <br />
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim
            dolore neque ipsa fuga rerum veritatis eaque at vel harum excepturi.
          </p>
          <br />
          {isAuthenticated ? (
            <Link to='/dashboard' className='btn'>
              back to dashboard
            </Link>
          ) : (
            <Link to='/login' className='btn'>
              Login to your account
            </Link>
          )}
        </div>
      </div>
    </Fragment>
  )
}

export default App
