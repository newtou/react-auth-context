import React, { createContext, useState, useEffect } from 'react'
import { getTokenFromLocalStorage } from '../localstorage/LocalStorage'

export const AuthContext = createContext()

export const AuthProvider = props => {
  const [isAuthenticated, setIsAuthenticated] = useState(false)

  useEffect(() => {
    const token = getTokenFromLocalStorage('token')
    if (token) {
      setIsAuthenticated(true)
    }
  }, [])

  return (
    <AuthContext.Provider value={[isAuthenticated, setIsAuthenticated]}>
      {props.children}
    </AuthContext.Provider>
  )
}
