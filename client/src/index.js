import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'
import AppRouter from './routes/appRoutes'
import { AuthProvider } from './context/AuthContext'

// I just thought this read better
const app = (
  <AuthProvider>
    <AppRouter></AppRouter>
  </AuthProvider>
)

ReactDOM.render(app, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
