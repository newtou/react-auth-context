import React from 'react'
import { Link } from 'react-router-dom'

function NotFound() {
  return (
    <div className='container--404'>
      <div className='content--404'>
        <h1>404 NOT FOUND</h1>
        <br />
        <hr />
        <br />
        <p>The page you were looking for is not found</p>
        <br />
        <Link to='/' className='link--404 btn'>
          Back to somewhere nice
        </Link>
      </div>
      <br />
    </div>
  )
}

export default NotFound
