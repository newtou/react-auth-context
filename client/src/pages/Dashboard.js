import React, { Fragment, useContext } from 'react'
import Header from '../components/Header'
import Dropzone from '../components/Dropzone'

function Dashboard({ history }) {
  return (
    <Fragment>
      <Header></Header>
      <div className='dashboard--container'>
        <div className='dashboard--content'>
          <h3>This is the dashboard page</h3>
          <br />
          <hr />
          <br />
          <div>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi
              consequuntur blanditiis, ut accusamus quae excepturi? Quaerat
              praesentium dolorem vitae vero!
            </p>
            <br />
            <hr />
            <br />
          </div>
          <div className='drop'>
            <Dropzone></Dropzone>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Dashboard
