import React from 'react'
import { Route, Switch, Router } from 'react-router-dom'

import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'
import history from './history'

import App from '../App'
import Login from '../components/Login'
import Register from '../components/Register'
import Dashboard from '../pages/Dashboard'
import NotFound from '../pages/NotFound'

function AppRoutes() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path='/' component={App}></Route>
        <PublicRoute path='/login' component={Login}></PublicRoute>
        <PublicRoute path='/register' component={Register}></PublicRoute>
        <PrivateRoute path='/dashboard' component={Dashboard}></PrivateRoute>
        <Route exact path='*' component={NotFound}></Route>
      </Switch>
    </Router>
  )
}

export default AppRoutes
