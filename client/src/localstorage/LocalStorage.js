import axios from 'axios'

export function saveTokenToLocalStorage(token) {
  try {
    const serializedToken = JSON.stringify(token)
    localStorage.setItem('token', serializedToken)
  } catch (error) {
    return false
  }
}

export function getTokenFromLocalStorage() {
  try {
    const token = localStorage.getItem('token')
    return token === null ? undefined : token
  } catch (error) {
    return undefined
  }
}

export function removeTokenFromLocalStorage() {
  try {
    return localStorage.removeItem('token')
  } catch (error) {
    return false
  }
}

export const login = async (email, password) => {
  const details = {
    email: email,
    password: password
  }
  const res = await axios.post('http://localhost:5000/api/login', details, {
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' }
  })
  saveTokenToLocalStorage(res.data)
  return res.data
}

export const register = async (name, email, password) => {
  const details = {
    name: name,
    email: email,
    password: password
  }
  const res = await axios.post('http://localhost:5000/api/register', details, {
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' }
  })
  saveTokenToLocalStorage(res.data)
  return res.data
}

export const logout = () => {
  removeTokenFromLocalStorage()
}
