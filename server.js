const express = require('express')
const http = require('http')
const path = require('path')
const mongoose = require('mongoose')
const cors = require('cors')

// access env variables
require('dotenv').config()

//env varaibles
const mongo_uri = process.env.MONGO_URI

const app = express()
// express middleware
app.use(express.json())
app.use(express.static(path.join(__dirname, 'client/build')))

// other middleware
app.use(cors())
//mongoose
mongoose.connect(
  mongo_uri,
  { useCreateIndex: true, useNewUrlParser: true },
  () => console.log('** connected to db **')
)

// router middleware
const authRouter = require('./routes/auth')
app.use('/api', authRouter)
// setting the port
let port
if (process.env.NODE_ENV === 'production') {
  port = process.env.PORT
} else {
  port = '5000'
}
app.set('port', port)
const server = http.createServer(app)

server.listen(port, () => console.log(`*** server running on port ${port} ***`))
